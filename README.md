# Endpoint Health, Inc. ARDS Phenotyper Package

## Overview

This package includes a K-means clustering model used to assign patients
diagnosted with ARDS to either a higher risk cluster (Cluster 1) or a lower
risk cluster (Cluster 2).


### Model Features

The following list describes the various model input features, including the
name of the column where each feature is expected to be found in the input CSV,
how the feature is calculated, unit of measurement, upper and lower limits, and
the list of models each feature is required in.

The model was trained using data within 24 hours prior to ARDS diagnosis/study
enrollment when diagnosis time and/or enrollment time are available, otherwise
the first time the PaO2/FiO2 ratio was under 300 after the start of mechanical
ventilation.

The "limits" specified below indicate the lower and upper physiological limits
used to filter outliers from the training/validation data.

In addition to the feature columns specified below, the input CSV file must
also include a unique record identifier column, which by default is expected to
be 'RecordID'.  This can be any unique value, e.g. a numeric index of the row.

* Arterial pH
  - column: ARTPHR
  - calculation: most recent value
  - unit: pH
  - limits: 6.75, 7.75
* Bicarbonate
  - column: BICARL
  - calculation: lowest value
  - unit: mEq/L
  - limits: 1.0, 50.0
* Creatinine
  - column: CREATR
  - calculation: most recent value
  - unit: mg/dL
  - limits: 0.1, 20.0
* FiO2 (fraction of inspired oxygen)
  - column: FIO2R
  - calculation: most recent value
  - unit: decimal
  - limits: 0.21, 1.0
* Heart rate
  - column: HRATER
  - calculation: most recent value
  - unit: beats/minute
  - limits: 20.0, 300.0
* Mean arterial pressure
  - column: MEANAPR
  - calculation: most recent value
  - unit: mm Hg
  - limits: 10.0, 400.0
* Respiration rate
  - column: RESPR
  - calculation: most recent value
  - unit: breaths/minute
  - limits: 1.0, 100.0
* PaO2 (partial pressure of oxygen)
  - column: PAO2R
  - calculation: most recent value
  - unit: mm Hg
  - limits: 30.0, 500.0
* Bilirubin
  - column: BILIH
  - calculation: maximum value
  - unit: mg/dL
  - limits: 0.1, 20.0


## Usage

### Prerequisites

This package may be used directly on any Windows, Linux, or OSX machine with
x86_64 architecture, or by utilizing Docker.  While docker introduces
additional complexity into the process, it guarantees an environment consistent
with the one used to build this package, and is therefore the recommended
option.

* Prerequisites for docker usage (recommended)
  - by utilizing docker, you can ensure this phenotyper package operates using the exact same operating system and libraries used to train and evaluate the models, although a bit of additional complexity is introduced.
  - if you have not already installed docker, see the following to get started: <https://docs.docker.com/get-docker/>
      * the recommended option is "Stable" - to see the differences between stable and edge, see: <https://docs.docker.com/docker-for-windows/faqs/#what-is-the-difference-between-the-stable-and-edge-versions-of-docker-desktop>
  - make sure you start docker before proceeding
    * Linux with systemd: `systemctl start docker`
    * Windows: Find application, launch
    * OSX: Applications -> Docker, launch
  - you can skip the getting started tutorial
  - once docker is installed and running, build the image with the following command (from within a terminal/shell, in this package's directory)
    * windows: `docker build --no-cache -t ards-phenotyper "%cd%"`
    * Linux/OSX: `docker build --no-cache -t ards-phenotyper "$(pwd)"`
* Prerequisites for direct usage (without Docker, advanced)
  - due to the use of the python pickle library, only python version 3.8 is supported in this mode
  - additional python packages/versions specified in the requirements.txt must be installed (e.g. pip install -r requirements.txt)
  - if you choose to use versions of the python libraries other than those specified in the requirements.txt file, this package may not operate properly (scikit-learn *must* be version 0.23.1)


### HTTP interface (with docker, recommended)

* the image must be built first (see docker prerequisites above)
* the curl package must be installed
  - you can check if curl is installed by typing "curl" at a command line and hit enter, which should produce a warning message similar to: `curl: try 'curl --help' or 'curl --manual' for more information`
  - curl can be downloaded from: https://curl.haxx.se/download.html

First, start the ARDS phenotyper docker image in the background:

```
docker run --rm -d --name ards-phenotyper -p 8787:80 ards-phenotyper:latest
```

Explanation of the docker command options:
* --rm indicates the docker container should be removed/deleted when finished
* -d indicates the container should be run in daemon/background mode
* --name ards-phenotyper assigns the name "ards-phenotyper" to the container, rather than using a randomly generated name
* -p 8787:80 maps port 8787 on your host machine to port 80 (http) on the docker container (8787 can be changed to the port number of your choosing, but the right side/80 must remain unchanged)
* ards-phenotyper:latest is the name of the image built in the docker prerequisites section above

Once the docker container is running, you can use curl to evaluate input CSV files, along with query parameters to specify model selection.

The default URL (no query parameters) will cause all models to be evaluated:
```
curl -XPOST "http://127.0.0.1:8787/" -F "input=@input.csv" -o output.csv
```

*replace input.csv with the path to your input file, and output.csv with the desired output path*

The output CSV file will include the RecordID column, and two columns for each model:
  - [model-name]-cluster: Cluster 1 or Cluster 2, indicating the associated phenotype.
  - [model-name]-error: error message indicating the reason results could not
    be inferred for that row.

If you used a column other than `RecordID` as the unique row identifier, you must specify the `record_id_column` query parameter in the request, e.g.:
```
curl -XPOST "http://127.0.0.1:8787/?record_id_column=patient_id" -F 'input=@input.csv' -o output.csv
```

To use specific models, you can specify the model=[model1,model2,...] query parameter in the URL, e.g.:
```
curl -XPOST "http://127.0.0.1:8787/?models=model_4_pf_train_2_2" -F 'input=@input.csv' -o output.csv
```

When you are finished with the phenotyper tool, you can remove the docker container by running the following:
```
docker kill ards-phenotyper
```

### CLI interface (direct/without docker)

The easiest method to use the CLI is to specify only the required parameters, leaving the others as the default.  An example of the default/standard usage would be:
```
python ards_phenotyper.py -i input.csv -o output.csv
```
*replace input.csv and output.csv with desired values*

By default, all models are enabled.

The output CSV file will include the RecordID column, and two columns for each model:
  - [model-name]-cluster: Cluster 1 or Cluster 2, indicating the associated phenotype.
  - [model-name]-error: error message indicating the reason results could not
    be inferred for that row.

There are several advanced options, which can be seen by running the following:
```
python ards_phenotyper.py --help
```

The most common usage for the advanced options would be to limit the selection of models (comma-separated) to evaluate, e.g.:
```
python ads_phenotyper.py -i input.csv -o output.csv -m model_4_pf_train_2_2
```

Advanced options:
---
```
usage: ards_phenotyper.py [-h] [-i INPUT] [-o OUTPUT] [-m {model_4_train_2_2,model_4_pf_train_2_2} [{model_4_train_2_2,model_4_pf_train_2_2} ...]]
                          [-r RECORD_ID] [-s]

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        input CSV file to infer results for (CLI mode only) (default: None)
  -o OUTPUT, --output OUTPUT
                        output path (CLI mode only) (default: None)
  -m {model_4_train_2_2,model_4_pf_train_2_2} [{model_4_train_2_2,model_4_pf_train_2_2} ...], --model {model_4_train_2_2,model_4_pf_train_2_2} [{model_4_train_2_2,model_4_pf_train_2_2} ...]
                        model to use (can be specified multiple times, CLI mode only) (default: ['model_4_train_2_2', 'model_4_pf_train_2_2'])
  -r RECORD_ID, --record-id RECORD_ID
                        name of the column used as unique row identifier (default: RecordID)
  -s, --server          run in server (http) mode (default: False)
```
---


### CLI interface (with docker, advanced)

*the image must be built first (see docker prerequisites above)*

In order to use the CLI interface with docker, you'll need to mount a local volume onto the docker instance in order to read the input file and write the output file.  In the example below, the input CSV file is expected to be within the same directory as this package's folder, which in this example is `C:\Users\example\Downloads\ards_phenotyper`

*this path must be replaced with the path you are using*

Run the ARDS phenotyper docker image with a volume mount, e.g.:
```
docker run --rm -it -v C:\Users\example\Downloads\ards_phenotyper:/data --entrypoint bash ards_phenotyper:latest
```

Once you have a bash terminal, invoke the command line interface as documented in the CLI interface README sections above.  Once finished, you can close the terminal (and therefore stop the docker container) by typing `exit` or ctrl-d.  The output file will be located in this package's directory (depending on the output path the CLI interface was invoked with).
